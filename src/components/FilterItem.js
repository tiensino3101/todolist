import { Component, React } from "react";

class FilterItem extends Component {
  constructor(props) {
    super(props);
    this.state = {
      filterStatus: -1, // -1 all, 0 xong, 1 chưa xong
    };
  }

  onChange = (e) => {
    let target = e.target;
    let name = target.name;
    let value = target.value;
    this.setState({
      [name]: value,
    });
    this.props.onFilter(Number(value));
  };

  render() {
    return (
      <div className="col-auto d-flex align-items-center">
        <label className="text-secondary my-2 pr-2 view-opt-label">
          Filter
        </label>
        <select
          className="custom-select custom-select-sm btn my-2"
          name="filterStatus"
          value={this.state.filterStatus}
          onChange={this.onChange}
        >
          <option value={-1} selected>
            All
          </option>
          <option value={2}>Deacive</option>
          <option value={1}>Active</option>
        </select>
      </div>
    );
  }
}

export default FilterItem;
