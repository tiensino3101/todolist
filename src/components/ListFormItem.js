import { Component, React } from "react";
import ListTaskItem from "./ListTaskItem";

class ListFormItem extends Component {
  render() {
    let { tasks } = this.props;

    let elements = tasks.map((task) => {
      return (
        <ListTaskItem
          key={task.id}
          task={task}
          onToggleStatus={this.props.onToggleStatus}
          onDeleteTask={this.props.onDeleteTask}
          onUpdate={this.props.onUpdate}
        />
      );
    });
    return <div className="col mx-auto">{elements}</div>;
  }
}

export default ListFormItem;
