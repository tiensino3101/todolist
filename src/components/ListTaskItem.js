import { Component, React } from "react";

class ListTaskItem extends Component {
  onToggleStatus = (id) => {
    this.props.onToggleStatus(id);
  };

  onDeleteTask = (id) => {
    this.props.onDeleteTask(id);
    // console.log(id);
  };

  onUpdate = (id) => {
    this.props.onUpdate(id);
    // console.log(id);
  };
  render() {
    let { task } = this.props;
    return (
      <div className="col mx-auto">
        {/* Todo Item 2 */}
        <div className="row px-3 align-items-center todo-item rounded">
          <div className="col-auto m-1 p-0 d-flex align-items-center">
            <h2
              className="m-0 p-0"
              onClick={() => this.onToggleStatus(task.id)}
            >
              <i
                className={
                  task.status
                    ? "fa fa-square-o text-primary btn m-0 p-0 d-none"
                    : "fa fa-square-o text-primary btn m-0 p-0"
                }
                data-toggle="tooltip"
                data-placement="bottom"
                title="Mark as complete"
              />
              {/* fa fa-square-o text-primary btn m-0 p-0 */}
              <i
                className={
                  task.status
                    ? "fa fa-check-square-o text-primary btn m-0 p-0"
                    : "fa fa-check-square-o text-primary btn m-0 p-0 d-none"
                }
                data-toggle="tooltip"
                data-placement="bottom"
                title="Mark as todo"
              />
              {/* "fa fa-check-square-o text-primary btn m-0 p-0 d-none" */}
            </h2>
          </div>
          <div className="col px-1 m-1 d-flex align-items-center">
            <input
              type="text"
              className="
              form-control form-control-lg
              border-0
              edit-todo-input
              bg-transparent
              rounded
              px-3
            "
              readOnly
              value={task.name}
              title={task.name}
            />
            <input
              type="text"
              className="
              form-control form-control-lg
              border-0
              edit-todo-input
              rounded
              px-3
              d-none
            "
              value={task.name}
            />
          </div>
          <div className="col-auto m-1 p-0 px-3">
            <div className="row">
              <div
                className="
                col-auto
                d-flex
                align-items-center
                rounded
                bg-white
                border border-warning
              "
              >
                <i
                  className="fa fa-hourglass-2 my-2 px-2 text-warning btn"
                  data-toggle="tooltip"
                  data-placement="bottom"
                  title
                  data-original-title="Due on date"
                />
                <h6 className="text my-2 pr-2"> {task.generateDate}</h6>
              </div>
            </div>
          </div>
          <div className="col-auto m-1 p-0 todo-actions">
            <div className="row d-flex align-items-center justify-content-end">
              <h5
                className="m-0 p-0 px-2"
                onClick={() => this.onUpdate(task.id)}
              >
                <i
                  className="fa fa-pencil text-info btn m-0 p-0"
                  data-toggle="tooltip"
                  data-placement="bottom"
                  title="Edit todo"
                />
              </h5>
              <h5
                className="m-0 p-0 px-2"
                onClick={() => this.onDeleteTask(task.id)}
              >
                <i
                  className="fa fa-trash-o text-danger btn m-0 p-0"
                  data-toggle="tooltip"
                  data-placement="bottom"
                  title="Delete todo"
                />
              </h5>
            </div>
            <div className="row todo-created-info">
              <div className="col-auto d-flex align-items-center pr-2">
                <i
                  className="fa fa-info-circle my-2 px-2 text-black-50 btn"
                  data-toggle="tooltip"
                  data-placement="bottom"
                  title
                  data-original-title="Created date"
                />
                <label className="date-label my-2 text-black-50">
                  {task.generateDate}
                </label>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default ListTaskItem;
