import { Component, React } from "react";

class AddItem extends Component {
  constructor(props) {
    super(props);
    this.state = {
      id: "",
      name: "",
      generateDate: "",
      status: false,
      taskEditting: false,
      // keyword: "",
    };
  }

  // componentDidUpdate(prevProps, prevState) {
  //   if (prevState.id !== this.state.id) {
  //     console.log(this.state.name);
  //     console.log("khac nhau");
  //     this.setState({
  //       name: this.state.name,
  //     });
  //   } else {
  //     console.log("giong nhau");
  //   }
  // }

  // static getDerivedStateFromProps(nextProps) {
  //   if (nextProps.taskEditting && nextProps.taskEditting.id) {
  //     // console.log(nextProps.taskEditting.id);
  //     return {
  //       id: nextProps.taskEditting.id,
  //       name: nextProps.taskEditting.name,
  //     };
  //     // return "true";
  //   }
  //   return null;
  // }
  componentWillReceiveProps = (nextProps) => {
    if (nextProps.taskEditting !== null && nextProps.taskEditting.id) {
      // console.log(nextProps);
      this.setState({
        id: nextProps.taskEditting.id,
        name: nextProps.taskEditting.name,
        generateDate: nextProps.taskEditting.generateDate,
        status: nextProps.taskEditting.status,
        taskEditting: true,
      });
    }
    if (this.state.taskEditting) {
      this.setState({
        id: "",
        name: "",
        generateDate: "",
        status: false,
        taskEditting: false,
      });
    }
  };

  onSearch = () => {
    this.props.onSearch(this.state.name);
    this.setState({
      name: "",
    });
  };

  onAddNewTask = (e) => {
    e.preventDefault();
    if (this.state.name !== "") {
      this.props.onAddNewTask(this.state);
      this.setState({
        name: "",
      });
    }
  };

  onChange = (e) => {
    let target = e.target;
    let name = target.name;
    let value = target.value;
    this.setState({
      [name]: value,
    });
  };

  render() {
    return (
      <div className="col col-11 mx-auto">
        <form
          className="
          row
          bg-white
          rounded
          shadow-sm
          p-2
          add-todo-wrapper
          align-items-center
          justify-content-center
        "
          onSubmit={this.onAddNewTask}
        >
          <div className="col">
            <input
              className="
              form-control form-control-lg
              border-0
              add-todo-input
              bg-transparent
              rounded
            "
              type="text"
              placeholder="Add new .."
              name="name"
              value={this.state.name}
              onChange={this.onChange}
            />
          </div>
          <div className="col-auto m-0 px-2 d-flex align-items-center">
            <label
              className="
              text-secondary
              my-2
              p-0
              px-1
              view-opt-label
              due-date-label
              d-none
            "
            >
              Due date not set
            </label>
            <i
              className="
              fa fa-calendar
              my-2
              px-1
              text-primary
              btn
              due-date-button
            "
              data-toggle="tooltip"
              data-placement="bottom"
              title="Set a Due date"
            />
            <i
              className="
              fa fa-calendar-times-o
              my-2
              px-1
              text-danger
              btn
              clear-due-date-button
              d-none
            "
              data-toggle="tooltip"
              data-placement="bottom"
              title="Clear Due date"
            />
          </div>
          <div className="col-auto px-0 mx-0 mr-2">
            <button
              type="button"
              className="btn btn-primary"
              onClick={this.onAddNewTask}
            >
              {this.state.id === "" ? "Add" : "Update"}
            </button>
            &nbsp;
            <button
              type="button"
              className="btn btn-primary"
              onClick={this.onSearch}
            >
              Search
            </button>
          </div>
        </form>
      </div>
    );
  }
}

export default AddItem;
