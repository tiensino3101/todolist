import { Component, React } from "react";

class SortItem extends Component {
  constructor(props) {
    super(props);
    this.state = {
      sort: {
        by: "name",
        value: -1,
      },
    };
  }

  handleChange = (event) => {
    this.setState({
      sort: {
        by: event.target.name,
        value: Number(event.target.value),
        // value: event.target.value,
      },
    });
    // console.log(this.state.sort);
    this.props.onSort(this.state.sort);
  };

  render() {
    return (
      <div className="col-auto d-flex align-items-center px-1 pr-3">
        <label className="text-secondary my-2 pr-2 view-opt-label">Sort</label>
        <select
          className="custom-select custom-select-sm btn my-2"
          name="name"
          value={this.state.value}
          onChange={this.handleChange}
        >
          <option value={-1} selected>
            A toi Z
          </option>
          <option value={1}>Z toi A</option>
        </select>
        <i
          className="fa fa fa-sort-amount-asc text-info btn mx-0 px-0 pl-1"
          data-toggle="tooltip"
          data-placement="bottom"
          title="Ascending"
        />
        <i
          className="
          fa fa fa-sort-amount-desc
          text-info
          btn
          mx-0
          px-0
          pl-1
          d-none
        "
          data-toggle="tooltip"
          data-placement="bottom"
          title="Descending"
        />
      </div>
    );
  }
}

export default SortItem;
