import "./App.css";
import { Component, React } from "react";
import AddItem from "./components/AddItem";
import FilterItem from "./components/FilterItem";
import SortItem from "./components/SortItem";
import ListFormItem from "./components/ListFormItem";

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      tasks: [],
      taskEditting: null,
      filterStatus: -1,
      sort: {
        by: "name",
        value: 1,
      },
      keyword: "",
    };
  }

  componentDidMount() {
    if (localStorage && localStorage.getItem("tasks")) {
      let tasks = JSON.parse(localStorage.getItem("tasks"));

      this.setState({
        tasks: tasks,
      });
    }
  }

  getCurrentDate = () => {
    const now = new Date();
    const [day, month, year, hour, minutes, second] = [
      `${now.getDate()}`.padStart(2, 0),
      now.getMonth() + 1,
      now.getFullYear(),
      `${now.getHours()}`.padStart(2, 0),
      `${now.getMinutes()}`.padStart(2, 0),
      `${now.getSeconds()}`.padStart(2, 0),
    ];
    let result = `${day}/${month}/${year}, ${hour}:${minutes}:${second}`;
    return result;
  };

  onAddNewTask = (data) => {
    var randomstring = require("randomstring");
    let { tasks } = this.state;

    if (data.id === "") {
      let task = {
        id: randomstring.generate(),
        name: data.name,
        status: false,
        generateDate: this.getCurrentDate(),
      };
      tasks.push(task);
    } else {
      let index = this.getIndexById(data.id);
      tasks[index].name = data.name;
      // console.log(tasks[index].name);
    }

    this.setState({
      tasks: tasks,
      taskEditting: null,
    });
    localStorage.setItem("tasks", JSON.stringify(tasks));
  };

  getIndexById = (id) => {
    let { tasks } = this.state;
    let result = -1;
    tasks.forEach((task, index) => {
      if (task.id === id) {
        result = index;
      }
    });
    return result;
  };

  onToggleStatus = (id) => {
    let { tasks } = this.state;
    let index = this.getIndexById(id);
    if (index !== -1) {
      tasks[index].status = !tasks[index].status;
    }
    this.setState({
      tasks: tasks,
    });
    localStorage.setItem("tasks", JSON.stringify(tasks));
  };

  onDeleteTask = (id) => {
    let { tasks } = this.state;
    let index = this.getIndexById(id);
    if (index !== -1) {
      tasks.splice(index, 1);
      this.setState({
        tasks: tasks,
      });
    }
    localStorage.setItem("tasks", JSON.stringify(tasks));
  };

  onUpdate = (id) => {
    let { tasks } = this.state;
    let index = this.getIndexById(id);
    let taskEditting = tasks[index];
    this.setState({
      taskEditting: taskEditting,
    });
  };

  onFilter = (value) => {
    this.setState({
      filterStatus: value,
    });
  };

  onSort = (data) => {
    this.setState({
      sort: {
        by: data.by,
        value: data.value,
      },
    });
    // console.log(value);
  };

  onSearch = (data) => {
    this.setState({
      keyword: data,
    });
  };

  render() {
    let { tasks, taskEditting, filterStatus, sort, keyword } = this.state;

    if (keyword) {
      tasks = tasks.filter((task) => {
        return task.name.toLowerCase().indexOf(keyword) !== -1;
      });
    }

    if (filterStatus) {
      tasks = tasks.filter((task) => {
        if (filterStatus === -1) {
          return tasks;
        } else if (filterStatus === 1) {
          return task.status === false;
        } else if (filterStatus === 2) {
          return task.status === true;
        }
      });
    }

    if (sort.by === "name") {
      tasks.sort((a, b) => {
        if (a.name > b.name) return sort.value;
        else if (a.name < b.name) return -sort.value;
        else return 0;
      });
    }

    return (
      <div className="container m-5 p-2 rounded mx-auto bg-light shadow">
        {/* App title section */}
        <div className="row m-1 p-4">
          <div className="col">
            <div className="p-1 h1 text-primary text-center mx-auto display-inline-block">
              <i className="fa fa-check bg-primary text-white rounded p-2" />
              <u>My Todo-s</u>
            </div>
          </div>
        </div>
        {/* Create todo section */}
        <div className="row m-1 p-3">
          {/* add comonent */}
          <AddItem
            // onGenerateID={this.onGenerateID}
            onAddNewTask={this.onAddNewTask}
            taskEditting={taskEditting}
            onSearch={this.onSearch}
          />
        </div>
        <div className="p-2 mx-4 border-black-25 border-bottom" />
        {/* View options section */}
        <div className="row m-1 p-3 px-5 justify-content-end">
          <FilterItem onFilter={this.onFilter} />
          <SortItem onSort={this.onSort} />
        </div>
        {/* Todo list section */}
        <div className="row mx-1 px-5 pb-3 w-80">
          <ListFormItem
            tasks={tasks}
            onToggleStatus={this.onToggleStatus}
            onDeleteTask={this.onDeleteTask}
            onUpdate={this.onUpdate}
          />
        </div>
      </div>
    );
  }
}

export default App;
